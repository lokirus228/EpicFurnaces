package com.songoda.epicfurnaces.furnace;

import com.songoda.epicfurnaces.EpicFurnacesPlugin;
import com.songoda.epicfurnaces.api.furnace.Furnace;
import com.songoda.epicfurnaces.api.furnace.Level;
import com.songoda.epicfurnaces.boost.BoostData;
import com.songoda.epicfurnaces.menus.OverviewMenu;
import com.songoda.epicfurnaces.utils.Debugger;
import com.songoda.epicfurnaces.utils.Methods;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.FurnaceSmeltEvent;
import org.bukkit.inventory.FurnaceInventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.RegisteredServiceProvider;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.util.*;

/**
 * Created by songoda on 3/7/2017.
 */
public class EFurnace implements Furnace {

    private final EpicFurnacesPlugin instance = EpicFurnacesPlugin.getInstance();
    private Location location;
    private Level level;
    private String nickname;
    private UUID placedBy;
    private int uses, toLevel, radiusOverheatLast, radiusFuelShareLast;
    private List<Location> radiusOverheat = new ArrayList<>();
    private List<Location> radiusFuelShare = new ArrayList<>();
    private List<String> accessList;
    private Map<String, Integer> cache = new HashMap<>();

    public EFurnace(Location location, Level level, String nickname, int uses, int tolevel, List<String> accessList, UUID placedBy) {
        this.location = location;
        this.level = level;
        this.uses = uses;
        this.toLevel = tolevel;
        this.nickname = nickname;
        this.placedBy = placedBy;
        this.accessList = accessList;
        this.syncName();
    }

    public EFurnace(Block block, Level level, String nickname, int uses, int tolevel, List<String> accessList, UUID placedBy) {
        this(block.getLocation(), level, nickname, uses, tolevel, accessList, placedBy);
    }

    public void openOverview(Player player) {
        if (!player.hasPermission("epicfurnaces.overview")) {
            return;
        }

        new OverviewMenu(instance, this, player).open(player);
    }

    public void plus(FurnaceSmeltEvent e) {
        try {
            Block block = location.getBlock();
            if (block.getType() != Material.FURNACE) return;

            this.uses++;
            this.toLevel++;

            int multi = instance.getConfig().getInt("Main.Level Cost Multiplier");

            if (level.getReward() == null) return;

            String reward = level.getReward();
            String[] amt = {"1", "1"};
            if (reward.contains(":")) {
                String[] rewardSplit = reward.split(":");
                reward = rewardSplit[0].substring(0, rewardSplit[0].length() - 1);
                if (rewardSplit[1].contains("-"))
                    amt = rewardSplit[1].split("-");
                else {
                    amt[0] = rewardSplit[1];
                    amt[1] = rewardSplit[0];
                }
            }


            int needed = ((multi * level.getLevel()) - toLevel) - 1;

            if (instance.getConfig().getBoolean("Main.Upgrade By Smelting Materials")
                    && needed <= 0
                    && instance.getConfig().contains("settings.levels.Level-" + (level.getLevel() + 1))) {
                toLevel = 0;
                level = instance.getLevelManager().getLevel(this.level.getLevel() + 1);
            }

            this.updateCook();

            FurnaceInventory i = (FurnaceInventory) ((InventoryHolder) block.getState()).getInventory();

            int num = Integer.parseInt(reward);
            double rand = Math.random() * 100;
            if (rand >= num
                    || e.getResult().getType().equals(Material.SPONGE)
                    || instance.getConfig().getBoolean("Main.No Rewards From Custom Recipes")
                    && instance.getFurnaceRecipeFile().getConfig().contains("Recipes." + i.getSmelting().getType().toString())) {
                return;
            }

            int r = Integer.parseInt(amt[0]);
            if (Integer.parseInt(amt[0]) !=
                    Integer.parseInt(amt[1].replace("%", "")))
                r = (int) (Math.random() * ((Integer.parseInt(amt[1].replace("%", "")) - Integer.parseInt(amt[0])))) + Integer.parseInt(amt[0]);

            BoostData boostData = instance.getBoostManager().getBoost(placedBy);
            r = r * (boostData == null ? 1 : boostData.getMultiplier());


            if (e.getResult() != null) {
                e.getResult().setAmount(e.getResult().getAmount() + r);
                return;
            }

            e.setResult(new ItemStack(e.getResult().getType(), r));
        } catch (Exception ex) {
            Debugger.runReport(ex);
        }
    }

    public void upgrade(String type, Player player) {
        try {
            if (!instance.getLevelManager().getLevels().containsKey(this.level.getLevel() + 1))
                return;

            int cost;
            if (type.equals("XP")) {
                cost = level.getCostExperiance();
            } else {
                cost = level.getCostEconomy();
            }
            Level level = instance.getLevelManager().getLevel(this.level.getLevel() + 1);

            if (type.equals("ECO")) {
                if (instance.getServer().getPluginManager().getPlugin("Vault") != null) {
                    RegisteredServiceProvider<Economy> rsp = instance.getServer().getServicesManager().getRegistration(net.milkbowl.vault.economy.Economy.class);
                    net.milkbowl.vault.economy.Economy econ = rsp.getProvider();
                    if (econ.has(player, cost)) {
                        econ.withdrawPlayer(player, cost);
                        upgradeFinal(level, player);
                    } else {
                        player.sendMessage(instance.getLocale().getMessage("event.upgrade.cannotafford"));
                    }
                } else {
                    player.sendMessage("Vault is not installed.");
                }
            } else if (type.equals("XP")) {
                if (player.getLevel() >= cost || player.getGameMode() == GameMode.CREATIVE) {
                    if (player.getGameMode() != GameMode.CREATIVE) {
                        player.setLevel(player.getLevel() - cost);
                    }
                    upgradeFinal(level, player);
                } else {
                    player.sendMessage(instance.getLocale().getMessage("event.upgrade.cannotafford"));
                }
            }
        } catch (Exception ex) {
            Debugger.runReport(ex);
        }
    }

    private void upgradeFinal(Level level, Player player) {
        try {
            this.level = level;
            syncName();
            if (instance.getLevelManager().getHighestLevel() != level) {
                player.sendMessage(instance.getLocale().getMessage("event.upgrade.success", level.getLevel()));
            } else {
                player.sendMessage(instance.getLocale().getMessage("event.upgrade.maxed", level.getLevel()));
            }
            Location loc = location.clone().add(.5, .5, .5);

            player.getWorld().spawnParticle(org.bukkit.Particle.valueOf(instance.getConfig().getString("Main.Upgrade Particle Type")), loc, 200, .5, .5, .5);
            if (instance.getConfig().getBoolean("Main.Use Sounds")) {
                if (instance.getLevelManager().getHighestLevel() == level) {
                    player.playSound(player.getLocation(), org.bukkit.Sound.ENTITY_PLAYER_LEVELUP, 0.6F, 15.0F);
                } else {
                    player.playSound(player.getLocation(), org.bukkit.Sound.ENTITY_PLAYER_LEVELUP, 2F, 25.0F);
                    player.playSound(player.getLocation(), org.bukkit.Sound.BLOCK_NOTE_BLOCK_CHIME, 2F, 25.0F);
                    Bukkit.getScheduler().scheduleSyncDelayedTask(instance, () -> player.playSound(player.getLocation(), org.bukkit.Sound.BLOCK_NOTE_BLOCK_CHIME, 1.2F, 35.0F), 5L);
                    Bukkit.getScheduler().scheduleSyncDelayedTask(instance, () -> player.playSound(player.getLocation(), org.bukkit.Sound.BLOCK_NOTE_BLOCK_CHIME, 1.8F, 35.0F), 10L);
                }
            }
        } catch (Exception ex) {
            Debugger.runReport(ex);
        }
    }

    private void syncName() {
        if (location.getBlock().getType() != Material.FURNACE) return;

        org.bukkit.block.Furnace furnace = (org.bukkit.block.Furnace) location.getBlock().getState();
        furnace.setCustomName(Methods.formatName(level.getLevel(), uses, false));
        furnace.update();
    }

    public void updateCook() {
        try {
            Block block = location.getBlock();
            if (block == null || block.getType() != Material.FURNACE) return;
            Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(instance, () -> {
                int num = getPerformanceTotal();

                if (num > 200) {
                    num = 200;
                }

                if (num != 0) {
                    BlockState bs = (block.getState()); // max is 200
                    ((org.bukkit.block.Furnace) bs).setCookTime(Short.parseShort(Integer.toString(num)));
                    bs.update();
                }
            }, 1L);
        } catch (Exception e) {
            Debugger.runReport(e);
        }
    }

    @Override
    public boolean addToAccessList(String string) {
        return accessList.add(string);
    }

    @Override
    public boolean removeFromAccessList(String string) {
        return accessList.remove(string);
    }

    @Override
    public void clearAccessList() {
        accessList.clear();
    }

    @Override
    public List<Location> getRadius(boolean overHeat) {
        if (overHeat)
            return radiusOverheat.isEmpty() ? null : Collections.unmodifiableList(radiusOverheat);
        else
            return radiusFuelShare.isEmpty() ? null : Collections.unmodifiableList(radiusFuelShare);

    }

    @Override
    public void addToRadius(Location location, boolean overHeat) {
        if (overHeat)
            radiusOverheat.add(location);
        else
            radiusFuelShare.add(location);

    }

    @Override
    public void clearRadius(boolean overHeat) {
        if (overHeat)
            radiusOverheat.clear();
        else
            radiusFuelShare.clear();
    }

    @Override
    public int getRadiusLast(boolean overHeat) {
        if (overHeat)
            return radiusOverheatLast;
        else
            return radiusFuelShareLast;
    }

    @Override
    public void setRadiusLast(int radiusLast, boolean overHeat) {
        if (overHeat)
            this.radiusOverheatLast = radiusLast;
        else
            this.radiusFuelShareLast = radiusLast;
    }

    @Override
    public List<UUID> getAccessList() {
        List<UUID> list = new ArrayList<>();
        for (String line : accessList) {
            String[] halfs = line.split(":");
            list.add(UUID.fromString(halfs[0]));
        }

        return list;
    }

    @Override
    public Level getLevel() {
        return level;
    }

    @Override
    public Location getLocation() {
        return location.clone();
    }

    @Override
    public String getNickname() {
        return nickname;
    }

    @Override
    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    @Override
    public List<String> getOriginalAccessList() {
        return Collections.unmodifiableList(accessList);
    }

    @Override
    public int getPerformanceTotal() {
        String equation = "(" + level.getPerformance() + " / 100) * 200";
        try {
            if (!cache.containsKey(equation)) {
                ScriptEngineManager mgr = new ScriptEngineManager();
                ScriptEngine engine = mgr.getEngineByName("JavaScript");
                int num = (int) Math.round(Double.parseDouble(engine.eval("(" + level.getPerformance() + " / 100) * 200").toString()));
                cache.put(equation, num);
                return num;
            } else {
                return cache.get(equation);
            }
        } catch (ScriptException e) {
            Debugger.runReport(e);
        }
        return 0;
    }

    @Override
    public UUID getPlacedBy() {
        return placedBy;
    }

    @Override
    public int getToLevel() {
        return toLevel;
    }

    @Override
    public int getUses() {
        return uses;
    }
}
