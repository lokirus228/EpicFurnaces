package com.songoda.epicfurnaces.api.furnace;

import java.util.Map;

public interface LevelManager {

    void addLevel(int level, int costExperiance, int costEconomy, int performance, String reward, int fuelDuration, int overheat, int fuelShare);

    Level getLevel(int level);

    Level getLowestLevel();

    Level getHighestLevel();

    boolean isLevel(int level);

    Map<Integer, Level> getLevels();

    void clear();
}
